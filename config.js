module.exports = {
  endpoint: 'https://gitlab.torproject.org/api/v4/',
  platform: 'gitlab',
  onboardingConfig: {
    extends: ['config:recommended', ':disableDependencyDashboard'],
  },
  vulnerabilityAlerts: {
      "labels": ["Security"]
  },
  dependencyDashboard: false,
  ignorePresets: [":dependencyDashboard"],
  persistRepoData: true,
};
