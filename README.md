# Renovate Cron

[Renovate](https://github.com/renovatebot/renovate) will automatically discover your dependencies and issue merge requests to update them. It adapts to your workflow, allows you to configure its behavior, and will autodetect settings when possible.

This project runs every 30 minutes via its [CI schedule](https://gitlab.torproject.org/micah/renovate-cron/-/pipeline_schedules), scanning its configuration file and performing its operations: scan the configured repositories, check if newer versions of dependencies exist, make MRs for available updates. The MRs will patch the files directly, and include Release Notes for the newer versions (if they are available).

By default:

    You'll get separate Pull Requests for each dependency
    Major updates are kept separate from non-major updates

However, you can configure any of these in your project's Renovate configuration.

## Adding renovate to your project

In order to add renovate to your project, simply invite the user @renovate-bot to your project, providing it with `developer` access

The next time renovate runs on its schedule it will create an [onboarding](https://github.com/renovatebot/tutorial#part-2---onboarding) MR in your project. This MR is there to help you understand Renovate and its default settings before Renovate starts running on your repository. The [onboarding](https://github.com/renovatebot/tutorial#part-2---onboarding) MR creates a configuration file called renovate.json, which contains Renovate’s default settings and can be modified during onboarding.

Renovate will not make any changes to your repo or raise MRs until after you finish the [onboarding](https://github.com/renovatebot/tutorial#part-2---onboarding) process.

Once you have merged the onboarding MR, renovate will begin to generate [update MRs](https://github.com/renovatebot/tutorial#part-3---getting-to-know-renovates-update-prs) to the most recent dependency version based on your configuration (Note: Update MRs will take some time to appear).

## Changing Renovate behavior

Renovate configuration can be modified by configuration in your project, global organization configurations and existing Renovate presets.

To change how renovate acts in your project, you can change the renovate.json in your project. For example, if you do not want the dependency dashboard, you can disable that by adding:

    "extends": ["config:base", ":disableDependencyDashboard"]
    
Practically everything can be [configured](https://docs.renovatebot.com/configuration-options/), see the [documentation](https://docs.renovatebot.com) to learn more about how to configure Renovate.

## Reducing noise

Ideally, the noise from renovate will reduce with time, and you will see it less often. You can help that along by doing some of the following to reduce 'noise', see [the renovate guide on reducing noise](https://docs.renovatebot.com/noise-reduction) for more information:

- **auto-merge**: you can set things to auto-merge based on different criteria, some examples: use the `:automergePatch` for patch version (v1.0.x) changes, or use the `:automergeMinor` preset for minor versions (v1.x.0), or auto-merge linters/testing packages. If you are going to "just press merge" on certain dependency updates, then consider setting those to automerge, and keep those that you know you will always want to review. The tests/CI will fail and not merge if something is wrong and requires your attention. Every time you select Merge on a Renovate MR without manually testing it, you should consider if you can enable automerge and save yourself the time in future. This works great for dev dependencies, that have great test coverage!
- **grouping**: you can group MRs, so you don't get one for each change. For example, you can group all the minor version updates. Or maybe there are a bunch of dependencies that have `eslint` in their name, they can all be grouped together [Check out the possibilities](https://docs.renovatebot.com/noise-reduction/#package-grouping)
- **disable certain dependencies**: maybe you have development dependencies, and you don't want renovate to bother you with those 
- **ignore paths**: eg. common ones like `node_modules`, `bower_components`, `vendor`, `examples`, `tests`, `fixtures`
- **labels**: labels can automatically be created for different cases
- **schedule and limit**: MRs are only done during "work hours" (`schedule` and `timezone`) and only so many per day, and only so many can be open at once. Or maybe the `eslint` group (see above) could be all scheduled to run once a month 

## Help!

Contact @micah for help.



